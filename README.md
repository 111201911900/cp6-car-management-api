# Binar Challange Cp6 
## Car Management API
di buat oleh sayyida Amira Muthia Dina

## cara menjalankan
1. cloning project di dalam folder yang di inginkan mengunakan terminal dan perintah dibawah
```
git clone https://gitlab.com/111201911900/cp6-car-management-api.git
```

2. instal semua dependensi yang di butuhkan mengunakan perintah di bawah

```properties
    yarn install
```

3. Setelah itu buat file .env yang isinya sama dengan [.env-example](.env-example) dan isi sesuai dengan milik anda.

4. buat database mengunakan perintah di bawah

```properties
    yarn db:create
```

5. Migrate semua data ke database mengunakan perintah di bawah

```properties
    yarn db:migrate
```
6. Migrate semua data ke database mengunakan perintah di bawah

```properties
    yarn db:seed
```
7. jalankan server dengan perintah di bawah

```properties
    yarn dev
```

## Database Management

Di dalam repository ini sudah terdapat beberapa script yang dapat digunakan dalam memanage database, yaitu:

- `yarn db:create` digunakan untuk membuat database
- `yarn db:drop` digunakan untuk menghapus database
- `yarn db:migrate` digunakan untuk menjalankan database migration
- `yarn db:seed` digunakan untuk melakukan seeding
- `yarn db:rollback` digunakan untuk membatalkan migrasi terakhir

## API

untuk melihat code API Cars [klik disini](app/controllers/api/v1/carController.js).

untuk melihat code API User [klik disini](app/controllers/api/v1/userController.js).

## Data User

### superadmin

```
  email: superadmin@gmail.com
  password: superadmin
```

### admin
```
  email: admin@gmail.com
  password: admin
```

#### member

```
  email: 111201911900@mhs.dinus.ac.id
  password: akupusing
```